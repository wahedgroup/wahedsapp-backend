<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTeam extends Model
{
    public function getRouteKeyName()
    {
        return 'team_id';
    }
}
