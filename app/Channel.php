<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/*
 * This model is used to represent a groupchat / server
 */

class Channel extends Model
{

    protected $guarded = ['team_id'];

    function team()
    {
        return $this->belongsTo('App\Team');
    }

    function webhooks()
    {
        return $this->hasMany('App\WebHook');
    }
}
