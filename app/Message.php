<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['message','user_id','team_id','channel_id'];

    public function sentByUser(){
        return $this->belongsTo('App\User');
    }
}
