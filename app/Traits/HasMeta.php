<?php


namespace App\Traits;


use Illuminate\Support\Facades\DB;

trait HasMeta
{

    public function metadata()
    {
        return $this->hasMany('App\UserMeta');
    }


    public function meta($key = null)
    {
        if (!is_null($key)) {
            return $this->metadata()->where('meta_key', '=', $key)->get()->first()->meta_value;
        }

        return $this->metadata;
    }

    public function update_meta($key, $newValue)
    {
        return DB::table('user_meta')->where('meta_key', $key)->where('user_id', $this->id)->update(['meta_value' => $newValue]);
    }

    public function delete_meta($key)
    {
        return DB::table('user_meta')->where('meta_key', $key)->where('user_id', $this->id)->delete();
    }

    public function create_meta($key, $value = null)
    {
        if (!is_null($value)) {
            DB::table('user_meta')->insert(["user_id" => $this->id, "meta_key" => $key, "meta_value" => $value, "created_at" => now(), "updated_at" => now()]);
        } else {
            DB::table('user_meta')->insert(["user_id" => $this->id, "meta_key" => $key, "meta_value" => "", "created_at" => now(), "updated_at" => now()]);
        }
    }
}
