<?php


namespace App\Traits;


use App\User;
use Illuminate\Support\Facades\DB;

trait HasFriends
{
    // friendship that I started
    function friends_of_mine()
    {
        return $this->belongsToMany('App\User', 'friends', 'user_id', 'friend_id')->withTimestamps()
            ->wherePivot('accepted', '=', 1) // to filter only accepted
            ->withPivot('accepted'); // or to fetch accepted value
    }

    // friendship that I was invited to
    function friend_of()
    {
        return $this->belongsToMany('App\User', 'friends', 'friend_id', 'user_id')->withTimestamps()
            ->wherePivot('accepted', '=', 1)
            ->withPivot('accepted');
    }

    /**
     * Get the list of users that want to become friends with this user
     *
     * @return mixed
     */
    function pending_friends()
    {
        return $this->belongsToMany('App\User', 'friends', 'friend_id', 'user_id')
            ->withTimestamps()
            ->wherePivot('accepted', '=', 0)
            ->withPivot('accepted');
    }

    /**
     * Sets the pending friends attribute on the user
     *
     * @return \Illuminate\Support\Collection
     */
    function getSentPendingFriendsAttribute()
    {
        return DB::table('friends')->where('user_id', '=', $this->id)
            ->where('accepted', '=', 0)
            ->select('friend_id')->get();
    }

    // accessor allowing you call $user->friends
    public function getFriendsAttribute()
    {
        if (!array_key_exists('friends', $this->relations)) $this->load_friends();

        return $this->getRelation('friends');
    }

    /**
     *
     */
    protected function load_friends()
    {
        if (!array_key_exists('friends', $this->relations)) {
            $friends = $this->merge_friends();

            $this->setRelation('friends', $friends);
        }
    }

    /**
     * Merge the friends of mine and friend of array
     *
     * @return mixed
     */
    protected function merge_friends()
    {
        return $this->friends_of_mine->merge($this->friend_of);
    }

    /**
     * Save the newly made friendship
     *
     * @param User $user
     * @return mixed
     */
    public function add_friend(User $user)
    {
        return $this->friends_of_mine()->save($user);
    }

    /**
     * Remove a friendship using user_id
     *
     * @param User $user
     * @return mixed
     */
    public function remove_friend(User $user)
    {
        if ($this->friends_of_mine->contains($user)) {
            return $this->friends_of_mine()->detach($user);
        } else {
            return $this->friend_of()->detach($user);
        }
    }

    /**
     * Accepts a friend using his user_id
     *
     * @param User $user
     * @return int
     */
    public function accept_friend(User $user)
    {
        return DB::table('friends')->where('user_id', '=', $user->id)
            ->where('friend_id', '=', $this->id)
            ->update(['accepted' => 1, 'updated_at' => now()]);
    }

    /**
     *  Accept all incoming friend requests
     */
    public function accept_all()
    {
        foreach ($this->pending_friends as $pf) {
            $this->accept_friend($pf);
        }
    }
}
