<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Team extends Model
{

    protected $fillable = ['team_name', 'invite_code', 'description', 'user_id'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($team) {
            $team->invite_code = Str::random(8);
        });
    }


    public function channels()
    {
        return $this->hasMany('App\Channel');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function getTeamRolesAttribute(){
        return Role::where('name', 'like', 'team.' . $this->id . '%')->get();
    }

    public function getTeamPermissionsAttribute(){
        return Permission::where('name', 'like', 'team.' . $this->id . '%')->get();
    }
}
