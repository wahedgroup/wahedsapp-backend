<?php

namespace App;

use App\Events\UserCreated;
use App\Http\Requests\Friends\PendingSentRequest;
use App\Traits\HasFriends;
use App\Traits\HasMeta;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use Notifiable, HasRoles, HasFriends, HasMeta;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar_url','last_seen_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
//            "key"=>"value"
        ];
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team')->withTimestamps();
    }

    public function messages(){
        return $this->hasMany('App\Message');
    }

    public function isPartOfTeam($team){
        if($team instanceof Team){
            return $this->teams->contains($team);
        }elseif (is_int($team)){
            return $this->teams->contains(Team::find($team));
        }
    }

    public function canJoinChannel($team_id, $channel_id){
        if ($this->hasPermissionTo('team.'.$team_id.'.channel.'.$channel_id)){
            return true;
        }
        return false;
    }


    public function getOwnerOfAttribute()
    {
        return Team::all()->where('user_id', '=', $this->id);
    }

    /**
     * Invite another user, found by its ID
     *
     * @param $inviteCode
     * @return bool|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function invite($inviteCode)
    {
        //TODO perform the necessary validations and sanitizations on invite_code
        try {
            $userToInvite = User::where('invite_code', '=', $inviteCode)->firstOrFail();
            if ($this->add_friend($userToInvite)) {
                return true;
            } else {
                return false;
            }
        } catch (ModelNotFoundException $e) {
            return response([], 404);
        }
    }

    public function defaultSettings()
    {
        return [
            "update_lastseen" => false,
        ];
    }


    public function setDefaultSettings()
    {
        foreach ($this->defaultSettings() as $key => $value){
            $this->metadata()->save(new UserMeta(['meta_key'=>$key , 'meta_value'=>$value]));
        }
    }
}
