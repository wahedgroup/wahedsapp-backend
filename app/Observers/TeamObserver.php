<?php

namespace App\Observers;

use App\Channel;
use App\Team;
use App\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class TeamObserver
{
    /**
     * Handle the team "created" event.
     *
     * @param \App\Team $team
     * @return void
     */
    public function created(Team $team)
    {
        if ($team->channels->isEmpty()) {
            $team->channels()->save(new Channel(['channel_name' => 'General', 'type' => 'text']));
        }

        /*
         * We are using this place to generate all the default roles and permissions for the server
         */

        Role::create(['name' => 'team.' . $team->id . '.User']);
        $owner = Role::create(['name' => 'team.' . $team->id . '.Owner']);

        $u = User::find($team->user_id);
        $u->assignRole($owner);



        Permission::create(['name' => 'team.' . $team->id . '.message.send']);
        Permission::create(['name' => 'team.' . $team->id . '.message.edit']);
        Permission::create(['name' => 'team.' . $team->id . '.message.remove']);
        Permission::create(['name' => 'team.' . $team->id . '.message.pin']);
        Permission::create(['name' => 'team.' . $team->id . '.channel.create']);
        Permission::create(['name' => 'team.' . $team->id . '.channel.remove']);
        Permission::create(['name' => 'team.' . $team->id . '.channel.edit']);
        Permission::create(['name' => 'team.' . $team->id . '.permissions.manage']);
        Permission::create(['name' => 'team.' . $team->id . '.roles.manage']);
        Permission::create(['name' => 'team.' . $team->id . '.webhook.manage']);
        Permission::create(['name' => 'team.' . $team->id . '.member.invite']);
        Permission::create(['name' => 'team.' . $team->id . '.member.remove']);

        $owner->givePermissionTo($team->teamPermissions);
    }

    /**
     * Handle the team "updated" event.
     *
     * @param \App\Team $team
     * @return void
     */
    public function updated(Team $team)
    {
        //
    }

    /**
     * Handle the team "deleted" event.
     *
     * @param \App\Team $team
     * @return void
     */
    public function deleted(Team $team)
    {
        Permission::query()->where('name', 'LIKE', 'team.' . $team->id . '%')->delete();
        Role::query()->where('name', 'LIKE', 'team.' . $team->id . '%')->delete();
    }

    /**
     * Handle the team "restored" event.
     *
     * @param \App\Team $team
     * @return void
     */
    public function restored(Team $team)
    {
        //
    }

    /**
     * Handle the team "force deleted" event.
     *
     * @param \App\Team $team
     * @return void
     */
    public function forceDeleted(Team $team)
    {
        //
    }
}
