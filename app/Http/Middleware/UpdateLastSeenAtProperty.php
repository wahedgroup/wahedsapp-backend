<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class UpdateLastSeenAtProperty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            if(auth()->user()->meta('update_lastseen') == 1){
                $user = Auth::user();
                $user->update(['last_seen_at'=>now()]);
            }
        }

        return $next($request);
    }
}
