<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DisconnectFromChannelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //TODO checken of de user in de channel zit

        $team = $this->get('team_id');

        return auth()->user()->teams()->contains($team);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            null,
        ];
    }
}
