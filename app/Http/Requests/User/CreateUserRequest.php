<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'name' => 'max:50|',
//            'biography' => 'string|max:500',
//            'email' => 'email:rfc|unique:users,email',
//            'avatar_url' => '',
//            'password' => '',
        ];
    }
}
