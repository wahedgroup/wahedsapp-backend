<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTeamRoleRequest;
use App\Http\Requests\UpdateTeamRoleRequest;
use App\Team;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class TeamRoleController extends Controller
{
    /**
     * Get all the roles for given team
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Team $team)
    {
        if (auth()->user()->can('team.' . $team->id . '.roles.manage')) {
            $roles = $team->teamRoles;
            return $roles;
        } else {
            return response('You do not have access to modify this resource', 403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeamRoleRequest $request, Team $team)
    {
        if(auth()->user()->can('team.'.$team->id.'.roles.manage')){
            $teamPrefix = "team." . $team->id;
            return Role::firstOrCreate([
                'name' => $teamPrefix . "." . ucfirst($request->get('name'))
            ]);
        }else{
            return response('You do not have access to modify this resource', 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeamRoleRequest $request, Team $team)
    {
        if(auth()->user()->can('team.'.$team->id.'.roles.manage')){
            $teamPrefix = "team." . $team->id;

        }else{
            return response('You do not have access to modify this resource', 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
