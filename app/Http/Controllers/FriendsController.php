<?php

namespace App\Http\Controllers;

use App\Http\Requests\AcceptFriendRequest;
use App\Http\Requests\Friends\DeleteFriendRequest;
use App\Http\Requests\Friends\InviteFriendRequest;
use App\Http\Requests\Friends\PendingSentRequest;
use App\User;

class FriendsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->friends;
    }

    /**
     * Invite a user by his invite_code
     */
    public function invite(InviteFriendRequest $request)
    {
        return auth()->user()->invite($request->get('invite_code'));
    }


    /*
     * Return a list of users that have sent you a friend request
     */
    public function pending()
    {
        return auth()->user()->pending_friends;
    }


    /**
     * Accept a user's friend request by their id
     */
    public function accept(AcceptFriendRequest $request)
    {
        return auth()->user()->accept_friend(User::find($request->get('friend_id')));
    }


    /**
     * Accept all pending requests
     */
    public function accept_all()
    {
        return auth()->user()->accept_all();
    }

    /**
     * Remove a friendship with a friend by friend_id
     *
     * @param DeleteFriendRequest $request
     * @return mixed
     */
    public function delete(DeleteFriendRequest $request)
    {
        return auth()->user()->remove_friend(User::find($request->get('friend_id')));
    }

    /**
     * Get all friend requests sent by the user
     */
    public function sent(PendingSentRequest $request)
    {
        return auth()->user()->sent_pending_friends;
    }


}
