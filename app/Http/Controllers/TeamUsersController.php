<?php

namespace App\Http\Controllers;

use App\Team;

class TeamUsersController extends Controller
{
    public function __invoke(Team $team)
    {
        if (auth()->user()->teams->pluck('id')->contains($team->id)) {
            //User is part of team thus allowed to see the other users in the team
            return $team->users;
        } else {
            return response("user ". auth()->id() . " can not see the members of this team");
        }
    }
}
