<?php

namespace App\Http\Controllers;

use App\WebHook;
use Illuminate\Http\Request;

class WebHookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\WebHook $endpoint
     * @return \Illuminate\Http\Response
     */
    public function show(WebHook $endpoint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\WebHook $endpoint
     * @return \Illuminate\Http\Response
     */
    public function edit(WebHook $endpoint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\WebHook $endpoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebHook $endpoint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\WebHook $endpoint
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebHook $endpoint)
    {
        //
    }
}
