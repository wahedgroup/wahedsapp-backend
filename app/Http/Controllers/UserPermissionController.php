<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddPermissionRequest;
use App\Http\Requests\DeletePermissionRequest;
use App\User;
use Spatie\Permission\Models\Permission;

class UserPermissionController extends Controller
{


    /**
     * Get all permissions for a user
     */
    public function index()
    {
        return auth()->user()->permissions;
    }

    public function givePermission(AddPermissionRequest $request)
    {
        $permission = Permission::find($request->get('permission_id'));
        $user = User::find($request->get('user_id'));

        //TODO think about what we want to send the user when we succesfully add new permission
        if ($user->givePermissionTo($permission)) {
            return response('Permission added', 200);
        } else {
            return response('There was an error while adding the permission', 500);
        }
    }

    public function removePermission(DeletePermissionRequest $request)
    {
        $permission = Permission::find($request->get('permission_id'));
        $user = User::find($request->get('user_id'));

        if ($user->can($permission->name)) {
            if ($user->revokePermissionTo($permission)) {
                return response('Permission Removed', 200);
            } else {
                return response('There was an error while removing the permission', 500);
            }
        } else {
            return response('The user did not have this permission', 404);
        }
    }


}
