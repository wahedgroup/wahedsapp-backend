<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Http\Requests\StoreChannelTeamRequest;
use App\Http\Requests\UpdateChannelTeamRequest;
use App\Team;

class ChannelTeamController extends Controller
{

    public function index(Team $team)
    {
        return $team->channels;
    }

    public function store(StoreChannelTeamRequest $request, Team $team)
    {
        if (!$team->channels->pluck('channel_name')->contains($request->get('channel_name'))) {
            $channel = $request->get('channel_name');
            $team->channels()->save(new Channel(array_merge(['team_id' => $team->id], $request->only(['channel_name', 'type']))));

            return response("Channel $channel has successfully been created", 201);
        } else {
            return response("A channel with that name already exists", 409);
        }
    }

    public function update(UpdateChannelTeamRequest $request, Team $team, Channel $channel)
    {
        if (!$channel->update($request->except('team_id'))) {
            return Response(null, 409);
        }
    }

    public function destroy(\Request $request, Team $team, Channel $channel)
    {
        //TODO check users permissions
        if (auth()->user()->can('team.' . $team->id . '.channel.remove')) {
            $channel->team()->dissociate();
            $channel->delete();

            return response('successfully removed channel');
        } else {
            return response('you are not authorized to perform this request', 401);
        }
    }


}
