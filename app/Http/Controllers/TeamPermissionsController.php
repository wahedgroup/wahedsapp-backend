<?php

namespace App\Http\Controllers;

use App\Team;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class TeamPermissionsController extends Controller
{
    public function index(Team $team)
    {
        if (auth()->user()->can('team.' . $team->id . '.permissions.manage')) {
            return $team->teamPermissions;
        } else {
            return response("You are not authorized to see the permissions for this server", 401);
        }
    }

    public function bindPermissionToRole(Team $team, Permission $p, Role $r)
    {
        if (auth()->user()->can('team.' . $team->id . '.roles.manage')) {
            return $r->givePermissionTo($p);
        }else{
            return response('You are not authorized to modify this resource', 401);
        }
    }

    public function unbindPermissionToRole(Team $team, Permission $p, Role $r)
    {
        if (auth()->user()->can('team.' . $team->id . '.roles.manage')) {
            return $r->revokePermissionTo($p);
        }else{
            return response('You are not authorized to modify this resource', 401);
        }
    }
}
