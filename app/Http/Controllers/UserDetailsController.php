<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;

class UserDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        //TODO unlimit these users
        return DB::table('users')->select(['name', 'biography', 'avatar_url', 'last_seen_at'])->limit(1)->get();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return User|User[]|array|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        return User::find($id)->only(['name', 'biography', 'avatar_url', 'last_seen_at']);
    }

    public function potential()
    {
        return auth()->user()->potential_friends;
    }
}
