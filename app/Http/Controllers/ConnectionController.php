<?php

namespace App\Http\Controllers;

use App\Events\ConnectToChannelEvent;
use App\Events\DisconnectFromChannelEvent;
use App\Http\Requests\ConnectionRequest;
use App\Http\Requests\DisconnectFromChannelRequest;
use Illuminate\Support\Facades\Event;

class ConnectionController extends Controller
{
    public function connect(ConnectionRequest $request)
    {
        Event::dispatch(new ConnectToChannelEvent($request));
        return true;


    }

    public function disconnect(DisconnectFromChannelRequest $request)
    {
        Event::dispatch(new DisconnectFromChannelEvent($request));
        return true;

    }
}
