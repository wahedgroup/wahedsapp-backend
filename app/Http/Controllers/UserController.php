<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return User[]|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasPermissionTo('see users')) {
            return User::all();
        }
        return response("You have no permission to perform this request", 401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return User|\Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (auth()->id() == $user->id || auth()->user()->can('see users')) {
            return $user;
        } else {
            return response("You have no permission to perform this request", 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return bool|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if (auth()->id()== $user->id || auth()->user()->can('update users')) {
            return $user->update($request->all());
        } else {
            return response("You have no permission to perform this request", 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return bool|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (auth()->id() == $user->id || auth()->user()->can('delete users')) {
            $user->delete();

            return response("User $user->id has succesfully been deleted", 401);
        } else {
            return response("You have no permission to perform this request", 401);
        }

    }
}
