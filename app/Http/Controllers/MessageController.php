<?php

namespace App\Http\Controllers;

use App\Events\NewMessageReceivedEvent;
use App\Http\Requests\StoreMessageRequest;
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMessageRequest $request)
    {
        $message = $request->get('message');
        $team_id = $request->get('team_id');
        $user_id = $request->get('user_id');
        $channel_id = $request->get('channel_id');

        Message::create(['message'=>$message,'team_id'=>$team_id,'user_id'=>$user_id,'channel_id'=>$channel_id]);

        event(new NewMessageReceivedEvent($message,$user_id,$team_id,$channel_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        return $message;
    }

    public function getMessagesAfter($team_id,$channel_id){
        return Message::query()->where('');
    }

}
