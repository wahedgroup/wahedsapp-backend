<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserTeamRequest;
use App\Team;
use Illuminate\Http\Response;

class UserTeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->teams;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserTeamRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserTeamRequest $request)
    {
        $invcode = $request->get('team_invite_code');
        $teamToJoin = Team::where('invite_code', $invcode)->first();


        if (!auth()->user()->teams->contains($teamToJoin)) {
            $teamToJoin->users()->save(auth()->user());
            return response($teamToJoin, 201);
        } else {
            return response([
                "errors" => [
                    "user " . auth()->user()->id . " is already part of team " . $teamToJoin->id
                ]
            ], 409);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Team $userteam
     * @return Response
     */
    public function destroy(Team $userteam)
    {
        if (auth()->user()->teams->contains($userteam)) {
            $userteam->users()->detach(auth()->user());
            return response([
                "message" => [
                    "user " . auth()->user()->id . " has left team $userteam->id"
                ]
            ], 201);
        } else {
            return response([
                "errors" => [
                    "user " . auth()->user()->id . " is not part of team " . $userteam->id
                ]
            ], 409);
        }
    }
}
