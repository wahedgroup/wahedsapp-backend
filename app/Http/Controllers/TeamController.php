<?php

namespace App\Http\Controllers;

use App\Http\Requests\Teams\StoreTeamRequest;
use App\Team;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TeamController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return auth()->user()->owner_of;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTeamRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(StoreTeamRequest $request)
    {
        $team = Team::create($request->all());
        return auth()->user()->teams()->save($team);
    }

    /**
     * Display the specified resource.
     *
     * @param Team $team
     * @return Team
     */
    public function show(Team $team)
    {
        return $team;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Team $team
     * @return bool
     */
    public function update(Request $request, Team $team)
    {
        return $team->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Team $team
     * @return bool|Application|ResponseFactory|Response
     * @throws Exception
     */
    public function destroy(Team $team)
    {
        if (auth()->user()->hasRole('team.' . $team->id . '.Owner')) {
            return $team->delete();
        } else {
            return response([], 403);
        }
    }
}
