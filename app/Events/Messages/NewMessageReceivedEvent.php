<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewMessageReceivedEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;
    public $team_id;
    public $channel_id;
    public $user_id;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message,$user_id, $team_id, $channel_id)
    {
        $this->dontBroadcastToCurrentUser();
        $this->message      = $message;
        $this->team_id      = $team_id;
        $this->user_id      = $user_id;
        $this->channel_id   = $channel_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("team.$this->team_id.channel.$this->channel_id");
    }
}
