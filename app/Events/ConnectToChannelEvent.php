<?php

namespace App\Events;

use App\Http\Requests\ConnectionRequest;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ConnectToChannelEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $team_id;
    public $channel_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ConnectionRequest $request)
    {
        $this->team_id = $request->get('team_id');
        $this->channel_id = $request->get('channel_id');


    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('team.'.$this->team_id.'.channel.'.$this->channel_id);
    }
}
