<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebHook extends Model
{
    protected $guarded = [];

    public function channel()
    {
        return $this->belongsTo('App\Channel');
    }
}
