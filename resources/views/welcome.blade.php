@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mt-5">
                    <div class="card-header">
                        <h1>Welcome to the WahedsApp platform.</h1>
                    </div>

                    <div class="card-body">
                        <p>We are very happy to welcome you!</p>
                        <p>Please login to start using the application</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
