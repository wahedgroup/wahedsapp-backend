<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Permission::create(['name'=>'send new pm','guard_name'=>'api']);
//        Permission::create(['name'=>'edit sent pm','guard_name'=>'api']);
//        Permission::create(['name'=>'delete sent pm','guard_name'=>'api']);
//        Permission::create(['name'=>'create new broadcastlist','guard_name'=>'api']);
//        Permission::create(['name'=>'send to broadcastlist','guard_name'=>'api']);
//        Permission::create(['name'=>'create new channel','guard_name'=>'api']);
//        Permission::create(['name'=>'edit existing channel','guard_name'=>'api']);
//        Permission::create(['name'=>'delete existing channel','guard_name'=>'api']);


        Permission::create(['name'=>'see users','guard_name'=>'api']);
        Permission::create(['name'=>'update users','guard_name'=>'api']);
        Permission::create(['name'=>'delete users','guard_name'=>'api']);
    }
}
