<?php

use App\Team;
use App\User;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u = User::first();
        $t = Team::create(['invite_code'=>'ABCDEFGH', 'team_name'=>'testserver', 'description'=>'super nice server','user_id'=> $u->id]);
        $u->teams()->save($t);
    }
}
