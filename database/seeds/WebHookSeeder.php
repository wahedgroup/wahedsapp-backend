<?php

use App\Channel;
use App\WebHook;
use Illuminate\Database\Seeder;

class WebHookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $e = new WebHook(['name'=>'Cpt. Hook','icon_url'=>"http://placehold.it/125x125","webhook_url"=>env("APP_URL")."/api/endpoint/".Str::uuid()]);
        $c = Channel::first();
        $c->webhooks()->save($e);
    }
}

