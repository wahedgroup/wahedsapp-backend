<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $u = User::create(['name'=> 'melchior kokernoot', 'email'=>'melchiorkokernoot@gmail.com','password'=>Hash::make('melchior')]);
        $u = User::create(['name'=> 'adminus pinus', 'email'=>'adminuspinus@gmail.com','password'=>Hash::make('admin')]);
        $u->email_verified_at = now();

        $this->call(UserSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(ChannelSeeder::class);
        $this->call(WebHookSeeder::class);
    }
}
