<?php

use App\Channel;
use App\Team;
use App\User;
use Illuminate\Database\Seeder;

class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $a = new Channel(['channel_name'=>'testChannel2', 'type'=>'text']);
        $b = new Channel(['channel_name'=>'testChannel3', 'type'=>'text']);
        $c = new Channel(['channel_name'=>'testChannel', 'type'=>'voice']);

        $t = Team::first();
        $t->channels()->saveMany([$a,$b,$c]);
    }
}
