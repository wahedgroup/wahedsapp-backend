<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

//Broadcast::channel('App.User.{id}', function ($user, $id) {
//    return (int) $user->id === (int) $id;
//});
//

Broadcast::channel('team.{team_id}.channel.{channel_id}', function ($user, $team_id, $channel_id){
    if($user->isPartOfTeam($team_id) && $user->canJoinChannel($team_id, $channel_id)){
        return true;
    }
    return false;
});
