<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the 'api' middleware group. Enjoy building your API!
|
*/
Route::get('/deployhook/message/{message}',     'DeploymentController@message');
Route::post('/deployhook/deploy',               'DeploymentController@deploy');

Route::group(['middleware' => ['api'], 'prefix' => 'auth'], function ($router) {
    Route::post('login',                        'AuthController@login')->name('apilogin');
    Route::post('register',                     'AuthController@register')->name('apiRegister');
    Route::get('me',                            'AuthController@me');
    Route::post('logout',                       'AuthController@logout');
    Route::post('refresh',                      'AuthController@refresh');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('teams',                    'TeamController')->except(['edit', 'create']);
    Route::resource('team.channel',             'ChannelTeamController');
    Route::resource('userteam',                 'UserTeamsController')->only(['index', 'store', 'destroy']);
    Route::resource('userdetails',              'UserDetailsController')->only(['index', 'show','update']);
    Route::resource('message',                  'MessageController');
    Route::resource('users',                    'UserController');

    Route::group(['prefix' => 'usersettings'], function () {
            Route::get('/',                     'UserSettingsController@index');
            Route::get('/{meta_key}',           'UserSettingsController@show');
    });

    Route::group(['prefix' => 'friend'], function () {
        Route::get('/',                         'FriendsController@index');
        Route::get('/pending',                  'FriendsController@pending');
        Route::get('/pending/sent',             'FriendsController@sent');
        Route::post('/invite',                  'FriendsController@invite');
        Route::post('/accept',                  'FriendsController@accept');
        Route::post('/delete',                  'FriendsController@delete');
    });

    Route::group(['prefix' => 'userpermission'], function () {
        Route::get('/',                         'UserPermissionController@index');
        Route::post('/addpermission',           'UserPermissionController@givePermission');
        Route::post('/removepermission',        'UserPermissionController@removePermission');
    });

    Route::group(['prefix' => 'teampermission'], function () {
        Route::get('/{team}/bind/{p}/{r}',      'TeamPermissionsController@bindPermissionToRole');
        Route::get('/{team}/undbind{p}/{r}',    'TeamPermissionsController@unbindPermissionToRole');
        Route::get('/{team}',                   'TeamPermissionsController@index');
    });

    Route::group(['prefix' => 'teamrole'], function () {
        Route::post('/{team}/create',           'TeamRoleController@store');
        Route::put('/{team}/{role}',            'TeamRoleController@update');//TODO
        Route::get('/{team}',                   'TeamRoleController@index');
    });

    Route::group(['middleware'=> ['api']], function(){
        Route::post('/channel/connect',         'ConnectionController@connect');
        Route::post('/channel/disconnect',      'ConnectionController@disconnect');
    });

    Route::group(['prefix' => 'teamuser'], function (){
        Route::get('/{team}',                   'TeamUsersController');
    });
});

Route::group(['prefix' => 'debug'], function () {
    Route::resource('teamd', 'DebugTeamsController');
    Route::post('debug', function (Request $request) {
        return $request->all();
    });
});
